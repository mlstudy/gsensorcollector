package com.borqs.tensorflow.step.db;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

import java.util.Date;

@Table("Count")
public class CountData {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("mMoveMode")
    private int mMoveMode;
    @Column("mSensorPartMode")
    private int mSensorPartMode;
    @Column("mStepCount")
    private int mStepCount;
    @Column("mDate")
    private Date mDate;

    public CountData() {

    }

    public CountData(int moveMode, int sensorPartMode, int stepCount, Date date) {
        mMoveMode = moveMode;
        mSensorPartMode = sensorPartMode;
        mStepCount = stepCount;
        mDate = date;
    }

    public int getId() {
        return id;
    }

    public int getMoveMode() {
        return mMoveMode;
    }

    public void setMoveMode(int moveMode) {
        mMoveMode = moveMode;
    }

    public int getSensorPartMode() {
        return mSensorPartMode;
    }

    public void setSensorPartMode(int sensorPartMode) {
        mSensorPartMode = sensorPartMode;
    }

    public int getStepCount() {
        return mStepCount;
    }

    public void setStepCount(int stepCount) {
        mStepCount = stepCount;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }
}
