package com.borqs.tensorflow.step.db;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;


@Table("Step")
public class StepData {

    // 指定自增，每个对象需要有一个主键
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;

    @Column("today")
    private String today;

    @Column("stepOfAccelerometer")
    private String stepOfAccelerometer;

    @Column("stepOfCountSensor")
    private String stepOfCountSensor;

    @Column("stepOfDetectorSensor")
    private String stepOfDetectorSensor;

    @Column("stepOfTensorFlow")
    private String stepOfTensorFlow;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getStepOfAccelerometer() {
        return stepOfAccelerometer;
    }

    public void setStepOfAccelerometer(String stepOfAccelerometer) {
        this.stepOfAccelerometer = stepOfAccelerometer;
    }

    public String getStepOfCountSensor() {
        return stepOfCountSensor;
    }

    public void setStepOfCountSensor(String stepOfCountSensor) {
        this.stepOfCountSensor = stepOfCountSensor;
    }

    public String getStepOfDetectorSensor() {
        return stepOfDetectorSensor;
    }

    public void setStepOfDetectorSensor(String stepOfDetectorSensor) {
        this.stepOfDetectorSensor = stepOfDetectorSensor;
    }

    public String getStepOfTensorFlow() {
        return stepOfTensorFlow;
    }

    public void setStepOfTensorFlow(String stepOfTensorFlow) {
        this.stepOfTensorFlow = stepOfTensorFlow;
    }


    @Override
    public String toString() {
        return "StepData{" +
                "id=" + id +
                ", today='" + today + '\'' +
                ", stepOfAccelerometer='" + stepOfAccelerometer + '\'' +
                ", stepOfCountSensor='" + stepOfCountSensor + '\'' +
                ", stepOfDetectorSensor='" + stepOfDetectorSensor + '\'' +
                ", stepOfTensorFlow" + stepOfTensorFlow + '\'' +
                '}';
    }
}
