package com.borqs.tensorflow.step;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.HashMap;
import java.util.Map;

import com.borqs.tensorflow.step.R;

import com.borqs.tensorflow.step.service.StepService;
import com.borqs.tensorflow.step.utils.SensorDateCacheUtil;
import com.borqs.tensorflow.step.utils.SharedPreferencesUtils;
import com.borqs.tensorflow.step.view.StepArcView;

/**
 * 记步主页
 */
public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int MAX_STEP_QTY = 30000;  //设置最大步數30000
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE };

    private static final String KEY_IS_DATA_SAVE = "is_data_save";
    private static final String KEY_MOVE_MODE = "move_mode";
    private static final String KEY_SENSOR_PART_MODE = "sensor_part_mode";

    private StepService mService;

    Map<StepService.SOURCE_ID, StepArcView> mStepArcViews = new HashMap<>();
    private SharedPreferencesUtils sp;

    private Switch mDataSaveSwitch;
    private Spinner mMoveModeSpinner;
    private Spinner mSensorPartModeSpinner;

    private boolean mIsDataSave;
    private int mMoveMode;
    private int mSensorPartMode;

    private void assignViews() {
        mStepArcViews.put(StepService.SOURCE_ID.ACCELEROMETER_SENSOR_ID, ((StepArcView)
                findViewById(R.id.accelerometer)).setTextString("acc"));
        mStepArcViews.put(StepService.SOURCE_ID.COUNT_SENSOR_ID, ((StepArcView) findViewById(R.id
                .countsensor)).setTextString("count"));
        mStepArcViews.put(StepService.SOURCE_ID.DETECTOR_SENSOR_ID, ((StepArcView) findViewById(R
                .id.detectorsensor)).setTextString("detector"));
        mStepArcViews.put(StepService.SOURCE_ID.TENSOR_FLOW_ID, ((StepArcView) findViewById(R.id
                .tensorflow)).setTextString("tf"));

        mDataSaveSwitch = (Switch) findViewById(R.id.switch1);
        mMoveModeSpinner = (Spinner) findViewById(R.id.spinner_move_mode);
        mSensorPartModeSpinner = (Spinner) findViewById(R.id.spinner_sensor_part_mode);

        ArrayAdapter<String> adapterMoveMode = new ArrayAdapter<>(this, R.layout.spinner_text_view);
        String[] moveMode = getResources().getStringArray(R.array.moveMode);
        adapterMoveMode.addAll(moveMode);
        adapterMoveMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMoveModeSpinner.setAdapter(adapterMoveMode);

        ArrayAdapter<String> adapterSensorPartMode = new ArrayAdapter<>(this, R.layout
                .spinner_text_view);
        String[] sensorPartMode = getResources().getStringArray(R.array.sensorPartMode);
        adapterSensorPartMode.addAll(sensorPartMode);
        adapterSensorPartMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSensorPartModeSpinner.setAdapter(adapterSensorPartMode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        verifyStoragePermissions(this);
        assignViews();
        initData();
        addListener();
    }

    private void verifyStoragePermissions(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                int permission = ActivityCompat.checkSelfPermission(activity,
                        "android.permission.WRITE_EXTERNAL_STORAGE");
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE,
                            REQUEST_EXTERNAL_STORAGE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void addListener() {
        mDataSaveSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mIsDataSave = b;
                if (sp != null) {
                    sp.setParam(KEY_IS_DATA_SAVE, mIsDataSave);
                }
                if (mService != null) {
                    mService.setDataSaveSwitch(mIsDataSave);
                }
            }
        });

        mMoveModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mMoveMode = i;
                if (sp != null) {
                    sp.setParam(KEY_MOVE_MODE, mMoveMode);
                }
                if (mService != null) {
                    mService.setMoveMode(mMoveMode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSensorPartModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSensorPartMode = i;
                if (sp != null) {
                    sp.setParam(KEY_SENSOR_PART_MODE, mSensorPartMode);
                }
                if (mService != null) {
                    mService.setSensorPartMode(mSensorPartMode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initData() {
        sp = new SharedPreferencesUtils(this);
        mIsDataSave = (Boolean) sp.getParam(KEY_IS_DATA_SAVE, false);
        mMoveMode = (Integer) sp.getParam(KEY_MOVE_MODE, SensorDateCacheUtil.MOVE_MODE_WALK);
        mSensorPartMode = (Integer) sp.getParam(KEY_SENSOR_PART_MODE, SensorDateCacheUtil
                .SENSOR_PART_MODE_CHEST);

        mDataSaveSwitch.setChecked(mIsDataSave);
        mMoveModeSpinner.setSelection(mMoveMode);
        mSensorPartModeSpinner.setSelection(mSensorPartMode);
        //设置当前步数为0
        for (StepService.SOURCE_ID id : StepService.SOURCE_ID.values()) {
            if (id != StepService.SOURCE_ID.SOURCE_ALL) {
                mStepArcViews.get(id).setCurrentCount(MAX_STEP_QTY, 0);
            }
        }
        setupService();
    }

    private boolean isBind = false;

    /**
     * 开启计步服务
     */
    private void setupService() {
        Intent intent = new Intent(this, StepService.class);
        startService(intent);
        isBind = bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

    }

    ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((StepService.StepBinder) service).getService();
            //设置初始化数据
            for (StepService.SOURCE_ID id : StepService.SOURCE_ID.values()) {
                if (id != StepService.SOURCE_ID.SOURCE_ALL) {
                    mStepArcViews.get(id).setCurrentCount(MAX_STEP_QTY, mService.getStepForSourceId(id));
                }
            }

            //设置步数监听回调
            mService.registerCallback(new UpdateUiCallBack() {
                @Override
                public void updateUi(StepService.SOURCE_ID id, int stepCount) {
                    mStepArcViews.get(id).setCurrentCount(MAX_STEP_QTY, stepCount);
                }
            });

            mService.setDataSaveSwitch(mIsDataSave);
            mService.setMoveMode(mMoveMode);
            mService.setSensorPartMode(mSensorPartMode);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBind=false;
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isBind) {
            this.unbindService(mServiceConnection);
        }
    }

}
