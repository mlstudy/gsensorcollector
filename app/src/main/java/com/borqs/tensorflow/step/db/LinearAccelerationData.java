package com.borqs.tensorflow.step.db;


import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

import java.util.Date;

@Table("LinearAccelerationData")
public class LinearAccelerationData {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("mEventX")
    private float mEventX;
    @Column("mEventY")
    private float mEventY;
    @Column("mEventZ")
    private float mEventZ;
    @Column("mDate")
    private Date mDate;

    public LinearAccelerationData(){

    }

    public LinearAccelerationData(float eventX,float eventY,float eventZ,Date date){
        mEventX=eventX;
        mEventY=eventY;
        mEventZ=eventZ;
        mDate=date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getmEventX() {
        return mEventX;
    }

    public void setmEventX(float eventX) {
        this.mEventX = eventX;
    }

    public float getmEventY() {
        return mEventY;
    }

    public void setmEventY(float eventY) {
        this.mEventY = eventY;
    }

    public float getmEventZ() {
        return mEventZ;
    }

    public void setmEventZ(float eventZ) {
        this.mEventZ = eventZ;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date date) {
        this.mDate = date;
    }
}
