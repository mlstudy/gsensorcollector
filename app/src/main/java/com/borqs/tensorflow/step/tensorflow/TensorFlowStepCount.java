package com.borqs.tensorflow.step.tensorflow;

import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import com.borqs.tensorflow.step.db.AccelerometerProcessedData;

public class TensorFlowStepCount {
    private static final String TAG = "TensorFlowStepCount";

    private StepChangedListener mListener;
    private TensorFlowInferenceInterface mInferenceInterface;

    private static final String INPUT_PEAK_NAME = "peak";
    private static final String INPUT_TROUGH_NAME = "trough";
    private static final String INPUT_PERIOD_NAME = "period";
    private static final String OUTPUT_NAME = "linear/head/predictions/probabilities";

    private static final int MSG_PROCESS_DATA = 1;

    // parameter from training data
    private static final float PEAK_MEAN = 13.4f;
    private static final float PEAK_STD = 4.4f;
    private static final float PEAK_MIN = 6.8f;
    private static final float PEAK_MAX = 20.0f;

    private static final float TROUGH_MEAN = 7.7f;
    private static final float TROUGH_STD = 2.1f;
    private static final float TROUGH_MIN = 0.6f;
    private static final float TROUGH_MAX = 12.0f;

    private static final float PERIOD_MEAN = 182.6f;
    private static final float PERIOD_STD = 64.1f;
    private static final float PERIOD_MIN = 64.0f;
    private static final float PERIOD_MAX = 280.0f;

    private int mCount;
    private float[] mOutput = new float[2];

    private ProcessHandler mHandler;
    private ProcessThread mProcessThread;
    private Handler mMainThreadHandler = new Handler(Looper.getMainLooper());

    private class ProcessThread extends Thread {
        ProcessThread() {
            super("TensorFlowStepCountProcessThread");
        }

        @Override
        public void run() {
            Looper.prepare();

            synchronized (TensorFlowStepCount.this) {
                mHandler = new ProcessHandler();
                TensorFlowStepCount.this.notify();
            }

            Looper.loop();
        }
    }

    private class ProcessHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_PROCESS_DATA:
                    AccelerometerProcessedData data = (AccelerometerProcessedData) msg.obj;
                    _processData(data);
                    break;
            }
        }
    }

    public TensorFlowStepCount(AssetManager assetManager, String modePath) {
        mInferenceInterface = new TensorFlowInferenceInterface(assetManager, modePath);
        createProcessHandler();
    }

    private void createProcessHandler() {
        mProcessThread = new ProcessThread();
        mProcessThread.start();

        synchronized (this) {
            while (mHandler == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    Log.e(TAG, "Interrupted while waiting on process handler.", e);
                }
            }
        }
    }

    public void initListener(StepChangedListener listener) {
        mListener = listener;
    }

    public void setStep(int initValue) {
        mCount = initValue;
        notifyListener();
    }

    private void notifyListener() {
        mMainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.stepChanged(mCount);
                }
            }
        });

    }

    public void processData(final AccelerometerProcessedData data) {
        Message msg = mHandler.obtainMessage(MSG_PROCESS_DATA, data);
        mHandler.sendMessage(msg);
    }

    private void _processData(AccelerometerProcessedData data) {
        float peak = data.getPeak();
        float trough = data.getTrough();
        float period = data.getPeriod();

        if (peak > PEAK_MAX)
            peak = PEAK_MAX;
        peak = (peak - PEAK_MEAN) / PEAK_STD;

        if (trough > TROUGH_MAX)
            trough = TROUGH_MAX;
        trough = (trough - TROUGH_MEAN) / TROUGH_STD;

        if (period > PERIOD_MAX)
            period = PERIOD_MAX;
        period = (period - PERIOD_MEAN) /PERIOD_STD;

        mInferenceInterface.feed(INPUT_PEAK_NAME, new float[]{peak}, 1);
        mInferenceInterface.feed(INPUT_TROUGH_NAME, new float[]{trough}, 1);
        mInferenceInterface.feed(INPUT_PERIOD_NAME, new float[]{period}, 1);

        mInferenceInterface.run(new String[]{OUTPUT_NAME}, false);
        mInferenceInterface.fetch(OUTPUT_NAME, mOutput);
        if (mOutput[1] > 0.5f) {
            mCount++;
            notifyListener();
        }
    }

    public interface StepChangedListener {
        void stepChanged(int step);
    }

}
