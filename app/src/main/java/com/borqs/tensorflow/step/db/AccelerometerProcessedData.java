package com.borqs.tensorflow.step.db;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

@Table("AccelerometerProcessed")
public class AccelerometerProcessedData {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("peak")
    private float mPeak;
    @Column("trough")
    private float mTrough;
    @Column("period")
    private long mPeriod;
    @Column("moveMode")
    private int mMoveMode;
    @Column("sensorPartMode")
    private int mSensorPartMode;
    @Column("flag")
    private int mFlag;

    public AccelerometerProcessedData() {

    }

    public AccelerometerProcessedData(float peak, float trough, long period, int moveMode, int
            sensorPartMode, int flag) {
        mPeak = peak;
        mTrough = trough;
        mPeriod = period;
        mMoveMode = moveMode;
        mSensorPartMode = sensorPartMode;
        mFlag = flag;

    }

    public int getId() {
        return id;
    }

    public float getPeak() {
        return mPeak;
    }

    public void setPeak(float peak) {
        mPeak = peak;
    }

    public float getTrough() {
        return mTrough;
    }

    public void setTrough(float trough) {
        mTrough = trough;
    }

    public long getPeriod() {
        return mPeriod;
    }

    public void setPeriod(long period) {
        mPeriod = period;
    }

    public int getMoveMode() {
        return mMoveMode;
    }

    public void setMoveMode(int moveMode) {
        mMoveMode = moveMode;
    }

    public int getSensorPartMode() {
        return mSensorPartMode;
    }

    public void setSensorPartMode(int sensorPartMode) {
        mSensorPartMode = sensorPartMode;
    }

    public int getFlag() {
        return mFlag;
    }

    public void setFlag(int flag) {
        mFlag = flag;
    }

    @Override
    public String toString() {
        return "peak " + mPeak + " trough " + mTrough + " period " + mPeriod + " moveMode " +
                mMoveMode + " sensorPartMode " + mSensorPartMode;
    }
}
