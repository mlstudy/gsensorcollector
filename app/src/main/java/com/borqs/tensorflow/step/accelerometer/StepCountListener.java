package com.borqs.tensorflow.step.accelerometer;


public interface StepCountListener {
    void countStep();
}
