package com.borqs.tensorflow.step.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.orhanobut.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.borqs.tensorflow.step.R;
import com.borqs.tensorflow.step.MainActivity;
import com.borqs.tensorflow.step.UpdateUiCallBack;
import com.borqs.tensorflow.step.accelerometer.StepCount;
import com.borqs.tensorflow.step.accelerometer.StepValuePassListener;
import com.borqs.tensorflow.step.db.CountData;
import com.borqs.tensorflow.step.db.GravityData;
import com.borqs.tensorflow.step.db.LinearAccelerationData;
import com.borqs.tensorflow.step.db.StepData;
import com.borqs.tensorflow.step.tensorflow.TensorFlowStepCount;
import com.borqs.tensorflow.step.utils.DbUtils;
import com.borqs.tensorflow.step.utils.SensorDateCacheUtil;

public class StepService extends Service implements SensorEventListener {
    private String TAG = "StepService";

    public enum SOURCE_ID {
        ACCELEROMETER_SENSOR_ID,
        COUNT_SENSOR_ID,
        DETECTOR_SENSOR_ID,
        TENSOR_FLOW_ID,
        SOURCE_ALL,
    }

    /**
     * 默认为30秒进行一次存储
     */
    private static int duration = 30 * 1000;
    /**
     * 当前的日期
     */
    private static String CURRENT_DATE = "";
    /**
     * 传感器管理对象
     */
    private SensorManager sensorManager;
    /**
     * 广播接受者
     */
    private BroadcastReceiver mBatInfoReceiver;
    /**
     * 保存记步计时器
     */
    private TimeCount time;
    /**
     * 当前所走的步数
     */
    private int mAccelerometerCurrentStep;
    private int mCountSensorCurrentStep;
    private int mDetectorSensorCurrentStep;
    private int mTensorFlowCurrentStep;
    /**
     * 每次第一次启动记步服务时是否从系统中获取了已有的步数记录
     */
    private boolean hasRecord = false;
    /**
     * 系统中获取到的已有的步数
     */
    private int hasStepCount = 0;
    /**
     * 上一次的步数
     */
    private int previousStepCount = 0;
    /**
     * 通知管理对象
     */
    private NotificationManager mNotificationManager;
    /**
     * 加速度传感器中获取的步数
     */
    private StepCount mStepCount;

    private TensorFlowStepCount mTensorFlowStepCount;

    /**
     * IBinder对象，向Activity传递数据的桥梁
     */
    private StepBinder stepBinder = new StepBinder();
    /**
     * 通知构建者
     */
    private NotificationCompat.Builder mBuilder;

    private SensorDateCacheUtil mDataCacheUtil;

    private PowerManager.WakeLock mWeakLock;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
        initNotification();
        initTodayData();
        initBroadcastReceiver();
        initSensorDatabaseUtil();
        new Thread(new Runnable() {
            public void run() {
                startStepDetector();
            }
        }).start();
        startTimeCount();
        acquireWakeLock();
    }

    private void initSensorDatabaseUtil() {
        mDataCacheUtil = new SensorDateCacheUtil();
    }

    /**
     * 获取当天日期
     *
     * @return
     */
    private String getTodayDate() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    /**
     * 初始化通知栏
     */
    private void initNotification() {
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("今日步数" + mTensorFlowCurrentStep + " 步")
                .setContentIntent(getDefalutIntent(Notification.FLAG_ONGOING_EVENT))
                .setWhen(System.currentTimeMillis())//通知产生的时间，会在通知信息里显示
                .setPriority(Notification.PRIORITY_DEFAULT)//设置该通知优先级
                .setAutoCancel(false)//设置这个标志当用户单击面板就可以让通知将自动取消
                .setOngoing(true)//ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,
                // 因此占用设备(如一个文件下载,同步操作,主动网络连接)
                .setSmallIcon(R.mipmap.run);
        Notification notification = mBuilder.build();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        startForeground(notifyId_Step, notification);
        Log.d(TAG, "initNotification()");
    }

    /**
     * 初始化当天的步数
     */
    private void initTodayData() {
        CURRENT_DATE = getTodayDate();
        DbUtils.createDb(this, "TensorFlowStepCount");
        DbUtils.getLiteOrm().setDebugged(false);
        //获取当天的数据，用于展示
        List<StepData> list = DbUtils.getQueryByWhere(StepData.class, "today", new
                String[]{CURRENT_DATE});
        if (list.size() == 0 || list.isEmpty()) {
            mAccelerometerCurrentStep = 0;
            mCountSensorCurrentStep = 0;
            mDetectorSensorCurrentStep = 0;
            mTensorFlowCurrentStep = 0;
        } else if (list.size() == 1) {
            Log.v(TAG, "StepData=" + list.get(0).toString());
            mAccelerometerCurrentStep = Integer.parseInt(list.get(0).getStepOfAccelerometer());
            mCountSensorCurrentStep = Integer.parseInt(list.get(0).getStepOfCountSensor());
            mDetectorSensorCurrentStep = Integer.parseInt(list.get(0).getStepOfDetectorSensor());
            mTensorFlowCurrentStep = Integer.parseInt(list.get(0).getStepOfTensorFlow());
        } else {
            Log.v(TAG, "出错了！");
        }
        if (mStepCount != null) {
            mStepCount.setSteps(mAccelerometerCurrentStep);
        }
        if (mTensorFlowStepCount != null) {
            mTensorFlowStepCount.setStep(mTensorFlowCurrentStep);
        }
        updateNotification(SOURCE_ID.SOURCE_ALL);
    }

    /**
     * 注册广播
     */
    private void initBroadcastReceiver() {
        final IntentFilter filter = new IntentFilter();
        // 屏幕灭屏广播
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        //关机广播
        filter.addAction(Intent.ACTION_SHUTDOWN);
        // 屏幕亮屏广播
        filter.addAction(Intent.ACTION_SCREEN_ON);
        // 屏幕解锁广播
//        filter.addAction(Intent.ACTION_USER_PRESENT);
        // 当长按电源键弹出“关机”对话或者锁屏时系统会发出这个广播
        // example：有时候会用到系统对话框，权限可能很高，会覆盖在锁屏界面或者“关机”对话框之上，
        // 所以监听这个广播，当收到时就隐藏自己的对话，如点击pad右下角部分弹出的对话框
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        //监听日期变化
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIME_TICK);

        mBatInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                String action = intent.getAction();
                if (Intent.ACTION_SCREEN_ON.equals(action)) {
                    Log.d(TAG, "screen on");
                } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                    Log.d(TAG, "screen off");
                    //改为60秒一存储
                    duration = 60000;
                } else if (Intent.ACTION_USER_PRESENT.equals(action)) {
                    Log.d(TAG, "screen unlock");
//                    save();
                    //改为30秒一存储
                    duration = 30000;
                } else if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(intent.getAction())) {
                    Log.i(TAG, " receive Intent.ACTION_CLOSE_SYSTEM_DIALOGS");
                    //保存一次
                    save();
                } else if (Intent.ACTION_SHUTDOWN.equals(intent.getAction())) {
                    Log.i(TAG, " receive ACTION_SHUTDOWN");
                    save();
                } else if (Intent.ACTION_DATE_CHANGED.equals(action)) {//日期变化步数重置为0
//                    Logger.d("重置步数" + StepDcretor.mAccelerometerCurrentStep);
                    save();
                    isNewDay();
                } else if (Intent.ACTION_TIME_CHANGED.equals(action)) {
                    //时间变化步数重置为0
                    save();
                    isNewDay();
                } else if (Intent.ACTION_TIME_TICK.equals(action)) {//日期变化步数重置为0
//                    Logger.d("重置步数" + StepDcretor.mAccelerometerCurrentStep);
                    save();
                    isNewDay();
                }
            }
        };
        registerReceiver(mBatInfoReceiver, filter);
    }

    /**
     * 监听晚上0点变化初始化数据
     */
    private void isNewDay() {
        String time = "00:00";
        if (time.equals(new SimpleDateFormat("HH:mm").format(new Date())) || !CURRENT_DATE.equals
                (getTodayDate())) {
            initTodayData();
        }
    }


    /**
     * 开始保存记步数据
     */
    private void startTimeCount() {
        if (time == null) {
            time = new TimeCount(duration, 1000);
        }
        time.start();
    }

    /**
     * 更新步数通知
     */
    private void updateNotification(SOURCE_ID sourceId) {
        //设置点击跳转
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Notification notification = mBuilder.setContentTitle(getResources().getString(R.string
                .app_name))
                .setContentText("今日步数" + mTensorFlowCurrentStep + " 步")
                .setWhen(System.currentTimeMillis())//通知产生的时间，会在通知信息里显示
                .setContentIntent(pi)
                .build();
        mNotificationManager.notify(notifyId_Step, notification);
        if (mCallback != null) {
            if (sourceId == SOURCE_ID.SOURCE_ALL) {
                for (SOURCE_ID id : SOURCE_ID.values()) {
                    if (id != SOURCE_ID.SOURCE_ALL) {
                        mCallback.updateUi(id, getStepForSourceId(id));
                    }
                }
            } else {
                mCallback.updateUi(sourceId, getStepForSourceId(sourceId));
            }
        }
        Log.d(TAG, "updateNotification()");
    }

    public int getStepForSourceId(SOURCE_ID sourceId) {
        if (sourceId == SOURCE_ID.SOURCE_ALL) {
            return 0;
        }

        int step = 0;
        switch (sourceId) {
            case ACCELEROMETER_SENSOR_ID:
                step = mAccelerometerCurrentStep;
                break;
            case COUNT_SENSOR_ID:
                step = mCountSensorCurrentStep;
                break;
            case DETECTOR_SENSOR_ID:
                step = mDetectorSensorCurrentStep;
                break;
            case TENSOR_FLOW_ID:
                step = mTensorFlowCurrentStep;
                break;
        }

        return step;
    }

    /**
     * UI监听器对象
     */
    private UpdateUiCallBack mCallback;

    /**
     * 注册UI更新监听
     *
     * @param paramICallback
     */
    public void registerCallback(UpdateUiCallBack paramICallback) {
        this.mCallback = paramICallback;
    }

    /**
     * 记步Notification的ID
     */
    int notifyId_Step = 100;
    /**
     * 提醒锻炼的Notification的ID
     */
    int notify_remind_id = 200;



    /**
     * @获取默认的pendingIntent,为了防止2.3及以下版本报错
     * @flags属性: 在顶部常驻:Notification.FLAG_ONGOING_EVENT
     * 点击去除： Notification.FLAG_AUTO_CANCEL
     */
    public PendingIntent getDefalutIntent(int flags) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, new Intent(), flags);
        return pendingIntent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return stepBinder;
    }

    /**
     * 向Activity传递数据的纽带
     */
    public class StepBinder extends Binder {

        /**
         * 获取当前service对象
         *
         * @return StepService
         */
        public StepService getService() {
            return StepService.this;
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    /**
     * 获取传感器实例
     */
    private void startStepDetector() {
        if (sensorManager != null) {
            sensorManager = null;
        }
        // 获取传感器管理器的实例
        sensorManager = (SensorManager) this
                .getSystemService(SENSOR_SERVICE);
        //android4.4以后可以使用计步传感器
        int VERSION_CODES = Build.VERSION.SDK_INT;
        if (VERSION_CODES >= 19) {
            addCountStepListener();
        } else {
            addAccelerometerListener();
            addTensorFlowStepCountListener(mStepCount);
        }
    }

    /**
     * 添加传感器监听
     * 1. TYPE_STEP_COUNTER API的解释说返回从开机被激活后统计的步数，当重启手机后该数据归零，
     * 该传感器是一个硬件传感器所以它是低功耗的。
     * 为了能持续的计步，请不要反注册事件，就算手机处于休眠状态它依然会计步。
     * 当激活的时候依然会上报步数。该sensor适合在长时间的计步需求。
     * <p>
     * 2.TYPE_STEP_DETECTOR翻译过来就是走路检测，
     * API文档也确实是这样说的，该sensor只用来监监测走步，每次返回数字1.0。
     * 如果需要长事件的计步请使用TYPE_STEP_COUNTER。
     */
    private void addCountStepListener() {
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor detectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        Sensor gravitySensor=sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        Sensor linearAccelerationSensor=sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        if (countSensor != null) {
            Log.i(TAG, "init Sensor.TYPE_STEP_COUNTER");
            sensorManager.registerListener(StepService.this, countSensor, SensorManager
                    .SENSOR_DELAY_NORMAL);
        }
        if (detectorSensor != null) {
            Log.i(TAG, "init Sensor.TYPE_STEP_DETECTOR");
            sensorManager.registerListener(StepService.this, detectorSensor, SensorManager
                    .SENSOR_DELAY_NORMAL);
        }
        if (gravitySensor != null) {
            Log.i(TAG, "init Sensor.TYPE_GRAVITY");
            sensorManager.registerListener(StepService.this, gravitySensor, SensorManager
                    .SENSOR_DELAY_NORMAL);
        }
        if (linearAccelerationSensor != null) {
            Log.i(TAG, "init Sensor.TYPE_LINEAR_ACCELERATION");
            sensorManager.registerListener(StepService.this, linearAccelerationSensor, SensorManager
                    .SENSOR_DELAY_NORMAL);
        }

        Log.i(TAG, "init Sensor.TYPE_ACCELEROMETER");
        addAccelerometerListener();
        addTensorFlowStepCountListener(mStepCount);
    }

    /**
     * 传感器监听回调
     * 记步的关键代码
     * 1. TYPE_STEP_COUNTER API的解释说返回从开机被激活后统计的步数，当重启手机后该数据归零，
     * 该传感器是一个硬件传感器所以它是低功耗的。
     * 为了能持续的计步，请不要反注册事件，就算手机处于休眠状态它依然会计步。
     * 当激活的时候依然会上报步数。该sensor适合在长时间的计步需求。
     * <p>
     * 2.TYPE_STEP_DETECTOR翻译过来就是走路检测，
     * API文档也确实是这样说的，该sensor只用来监监测走步，每次返回数字1.0。
     * 如果需要长事件的计步请使用TYPE_STEP_COUNTER。
     *
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        int sensorType = event.sensor.getType();
        float[] gravityValues = new float[3];
        float[] linearAccelerationValues= new float[3];
        if (sensorType == Sensor.TYPE_STEP_COUNTER) {
            //获取当前传感器返回的临时步数
            int tempStep = (int) event.values[0];
            //首次如果没有获取手机系统中已有的步数则获取一次系统中APP还未开始记步的步数
            if (!hasRecord) {
                hasRecord = true;
                hasStepCount = tempStep;
            } else {
                //获取APP打开到现在的总步数=本次系统回调的总步数-APP打开之前已有的步数
                int thisStepCount = tempStep - hasStepCount;
                //本次有效步数=（APP打开后所记录的总步数-上一次APP打开后所记录的总步数）
                int thisStep = thisStepCount - previousStepCount;
                //总步数=现有的步数+本次有效步数
                mCountSensorCurrentStep += (thisStep);
                //记录最后一次APP打开到现在的总步数
                previousStepCount = thisStepCount;

                mDataCacheUtil.saveCountData(new CountData(mDataCacheUtil.getMoveMode(),
                        mDataCacheUtil.getSensorPartMode(), mCountSensorCurrentStep, new Date
                        (event.timestamp / 1000000)));
            }
            updateNotification(SOURCE_ID.COUNT_SENSOR_ID);
            Logger.d("tempStep" + tempStep);
        } else if (sensorType == Sensor.TYPE_STEP_DETECTOR) {
            if (event.values[0] == 1.0) {
                mDetectorSensorCurrentStep++;
            }
            updateNotification(SOURCE_ID.DETECTOR_SENSOR_ID);
        } else if(sensorType==Sensor.TYPE_GRAVITY){
            for (int i = 0; i < 3; i++) {
                gravityValues[i] = event.values[i];
            }
            mDataCacheUtil.saveGravityData(new GravityData(gravityValues[0], gravityValues[1], gravityValues[2], new Date(event
                    .timestamp / 1000000)));
            Log.d(TAG, "onSensorChanged--------------------------------: gravitysensor data saved");
        }else if(sensorType==Sensor.TYPE_LINEAR_ACCELERATION){
            for (int i = 0; i < 3; i++) {
                linearAccelerationValues[i] = event.values[i];
            }
            mDataCacheUtil.saveLinearAccelerationData(new LinearAccelerationData(linearAccelerationValues[0],linearAccelerationValues[1],linearAccelerationValues[2],new Date(event.timestamp/1000000)));
            Log.d(TAG, "onSensorChanged--------------------------------: linearAcceleration data saved");
        }

    }

    /**
     * 通过加速度传感器来记步
     */
    private void addAccelerometerListener() {
        mStepCount = new StepCount(mDataCacheUtil);
        mStepCount.setSteps(mAccelerometerCurrentStep);
        // 获得传感器的类型，这里获得的类型是加速度传感器
        // 此方法用来注册，只有注册过才会生效，参数：SensorEventListener的实例，Sensor的实例，更新速率
        Sensor sensor = sensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        boolean isAvailable = sensorManager.registerListener(mStepCount.getStepDetector(), sensor,
                SensorManager.SENSOR_DELAY_UI);
        mStepCount.initListener(new StepValuePassListener() {
            @Override
            public void stepChanged(int steps) {
                mAccelerometerCurrentStep = steps;
                updateNotification(SOURCE_ID.ACCELEROMETER_SENSOR_ID);
            }
        });
        if (isAvailable) {
            Log.v(TAG, "加速度传感器可以使用");
        } else {
            Log.v(TAG, "加速度传感器无法使用");
        }
    }

    /**
     * 记录重力加速度传感器数据
     */


    private void addTensorFlowStepCountListener(StepCount stepCount) {
        mTensorFlowStepCount = new TensorFlowStepCount(getAssets(), "saved_model.pb");
        mTensorFlowStepCount.setStep(mTensorFlowCurrentStep);
        mTensorFlowStepCount.initListener(new TensorFlowStepCount.StepChangedListener() {
            @Override
            public void stepChanged(int step) {
                mTensorFlowCurrentStep = step;
                updateNotification(SOURCE_ID.TENSOR_FLOW_ID);
            }
        });

        stepCount.getStepDetector().setTensorFlowStepCount(mTensorFlowStepCount);
    }




    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * 保存记步数据
     */
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            // 如果计时器正常结束，则开始计步
            time.cancel();
            save();
            startTimeCount();
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

    }

    /**
     * 保存记步数据
     */
    private void save() {
        int tempAccelerometerStep = mAccelerometerCurrentStep;
        int tempCountSensorStep = mCountSensorCurrentStep;
        int tempDetectorStep = mDetectorSensorCurrentStep;
        int tempTensorFlowStep = mTensorFlowCurrentStep;

        List<StepData> list = DbUtils.getQueryByWhere(StepData.class, "today", new
                String[]{CURRENT_DATE});
        if (list.size() == 0 || list.isEmpty()) {
            StepData data = new StepData();
            data.setToday(CURRENT_DATE);
            data.setStepOfAccelerometer(tempAccelerometerStep + "");
            data.setStepOfCountSensor(tempCountSensorStep + "");
            data.setStepOfDetectorSensor(tempDetectorStep + "");
            data.setStepOfTensorFlow(tempTensorFlowStep + "");
            DbUtils.insert(data);
        } else if (list.size() == 1) {
            StepData data = list.get(0);
            data.setStepOfAccelerometer(tempAccelerometerStep + "");
            data.setStepOfCountSensor(tempCountSensorStep + "");
            data.setStepOfDetectorSensor(tempDetectorStep + "");
            data.setStepOfTensorFlow(tempTensorFlowStep + "");
            DbUtils.update(data);
        } else {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //取消前台进程
        stopForeground(true);
        DbUtils.closeDb();
        unregisterReceiver(mBatInfoReceiver);
        releaseWakeLock();
        Logger.d("stepService关闭");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void setMoveMode(int mode) {
        if (mDataCacheUtil != null) {
            mDataCacheUtil.setMoveMode(mode);
        }
    }

    public void setSensorPartMode(int mode) {
        if (mDataCacheUtil != null) {
            mDataCacheUtil.setSensorPartMode(mode);
        }
    }

    public void setDataSaveSwitch(boolean isDataSave) {
        if (mDataCacheUtil != null) {
            mDataCacheUtil.setDataSaveSwitch(isDataSave);
        }
    }

    private void acquireWakeLock() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWeakLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        mWeakLock.acquire();
    }

    private void releaseWakeLock() {
        if (mWeakLock != null) {
            mWeakLock.release();
            mWeakLock = null;
        }
    }
}
