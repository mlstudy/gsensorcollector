package com.borqs.tensorflow.step.db;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

import java.util.Date;

@Table("Accelerometer")
public class AccelerometerData {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("mMoveMode")
    private int mMoveMode;
    @Column("mSensorPartMode")
    private int mSensorPartMode;
    @Column("mEventX")
    private float mEventX;
    @Column("mEventY")
    private float mEventY;
    @Column("mEventZ")
    private float mEventZ;
    @Column("mDate")
    private Date mDate;

    public AccelerometerData() {

    }

    public AccelerometerData(int moveMode, int sensorPartMode, float eventX, float eventY, float
            eventZ, Date date) {
        mMoveMode = moveMode;
        mSensorPartMode = sensorPartMode;
        mEventX = eventX;
        mEventY = eventY;
        mEventZ = eventZ;
        mDate = date;
    }

    public int getId() {
        return id;
    }

    public int getMoveMode() {
        return mMoveMode;
    }

    public void setMoveMode(int moveMode) {
        mMoveMode = moveMode;
    }

    public int getSensorPartMode() {
        return mSensorPartMode;
    }

    public void setSensorPartMode(int sensorPartMode) {
        mSensorPartMode = sensorPartMode;
    }

    public float getEventX() {
        return mEventX;
    }

    public void setEventX(float eventX) {
        mEventX = eventX;
    }

    public float getEventY() {
        return mEventY;
    }

    public void setEventY(float eventY) {
        mEventY = eventY;
    }

    public float getEventZ() {
        return mEventZ;
    }

    public void setEventZ(float eventZ) {
        mEventZ = eventZ;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }
}
