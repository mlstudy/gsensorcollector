package com.borqs.tensorflow.step.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.borqs.tensorflow.step.db.AccelerometerData;
import com.borqs.tensorflow.step.db.AccelerometerProcessedData;
import com.borqs.tensorflow.step.db.CountData;
import com.borqs.tensorflow.step.db.GravityData;
import com.borqs.tensorflow.step.db.LinearAccelerationData;

/**
 * this class is enabled after DbUtils.createDb has called
 */
public class SensorDateCacheUtil {
    private static final String TAG = "StepService";
    private static final int ACCELEROMETER_SAVE_SIZE = 100;
    private static final int COUNT_SAVE_SIZE = 10;
    private static final int ACCELEROMETER_PROCESSED_SAVE_SIZE = 10;
    private static final int GRAVITY_SAVE_SIZE=100;
    private static final int LINEAR_ACCELERATION_SAVE_SIZE=100;

    private static final int MSG_SAVE_AND_CLEAR_DATA = 1;

    // mode number must is same as item index of moveMode in string.xml
    public static final int MOVE_MODE_WALK = 0;
    public static final int MOVE_MODE_RUN = 1;
    public static final int MOVE_MODE_STATIC = 2;

    // mode number must is same as item index of sensorPartMode in string.xml
    public static final int SENSOR_PART_MODE_CHEST = 0;
    public static final int SNESOR_PART_MODE_ARM = 1;
    public static final int SENSOR_PART_MODE_THIGH = 2;

    private ProcessHandler mHandler;
    private ProcessThread mProcessThread;

    private List<AccelerometerData> mAccelerometerDataCache1;
    private List<AccelerometerData> mAccelerometerDataCache2;
    private List<CountData> mCountDataCache1;
    private List<CountData> mCountDataCache2;
    private List<AccelerometerProcessedData> mAccelerometerProcessedDataCache1;
    private List<AccelerometerProcessedData> mAccelerometerProcessedDataCache2;
    private List<GravityData> mGravityDataCache1;
    private List<GravityData> mGravityDataCache2;
    private List<LinearAccelerationData> mLinearAccelerationDataCache1;
    private List<LinearAccelerationData> mLinearAccelerationDataCache2;

    private List<AccelerometerData> mCurrentAccelerometerDataCache;
    private List<CountData> mCurrentCountDataCache;
    private List<AccelerometerProcessedData> mCurrentAccelerometerProcessedDataCache;
    private List<GravityData> mCurrentGravityDataCache;
    private List<LinearAccelerationData> mCurrentLinearAccelerationDataCache;

    private boolean mIsDataSave;
    private int mMoveMode;
    private int mSensorPartMode;

    private class ProcessThread extends Thread {
        ProcessThread() {
            super("CacheUtilProcessThread");
        }

        @Override
        public void run() {
            Looper.prepare();

            synchronized (SensorDateCacheUtil.this) {
                mHandler = new ProcessHandler();
                SensorDateCacheUtil.this.notify();
            }

            Looper.loop();
        }
    }

    private class ProcessHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAVE_AND_CLEAR_DATA:
                    synchronized (msg.obj) {
                        List<?> list = (List<?>) msg.obj;
                        if (mIsDataSave) {
                            DbUtils.insertAll(list);
                        }
                        list.clear();
                    }
                    break;
            }
        }
    }

    public SensorDateCacheUtil() {
        mAccelerometerDataCache1 = new ArrayList<>();
        mAccelerometerDataCache2 = new ArrayList<>();
        mCountDataCache1 = new ArrayList<>();
        mCountDataCache2 = new ArrayList<>();
        mAccelerometerProcessedDataCache1 = new ArrayList<>();
        mAccelerometerProcessedDataCache2 = new ArrayList<>();
        mGravityDataCache1=new ArrayList<>();
        mGravityDataCache2=new ArrayList<>();
        mLinearAccelerationDataCache1=new ArrayList<>();
        mLinearAccelerationDataCache2=new ArrayList<>();

        mCurrentAccelerometerDataCache = mAccelerometerDataCache1;
        mCurrentCountDataCache = mCountDataCache1;
        mCurrentAccelerometerProcessedDataCache = mAccelerometerProcessedDataCache1;
        mCurrentGravityDataCache=mGravityDataCache1;
        mCurrentLinearAccelerationDataCache=mLinearAccelerationDataCache1;


        mMoveMode = 0;
        mSensorPartMode = 0;

        createProcessHandler();
    }

    private void createProcessHandler() {
        mProcessThread = new ProcessThread();
        mProcessThread.start();

        synchronized (this) {
            while (mHandler == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    Log.e(TAG, "Interrupted while waiting on process handler.", e);
                }
            }
        }
    }


    public void saveAccelerometerData(AccelerometerData data) {
        synchronized (mCurrentAccelerometerDataCache) {
            mCurrentAccelerometerDataCache.add(data);
        }

        if (mCurrentAccelerometerDataCache.size() >= ACCELEROMETER_SAVE_SIZE) {
            if (mCurrentAccelerometerDataCache == mAccelerometerDataCache1) {
                mCurrentAccelerometerDataCache = mAccelerometerDataCache2;
                saveAndClearCache(mAccelerometerDataCache1);
            } else if (mCurrentAccelerometerDataCache == mAccelerometerDataCache2) {
                mCurrentAccelerometerDataCache = mAccelerometerDataCache1;
                saveAndClearCache(mAccelerometerDataCache2);
            }
        }
    }


    public void saveCountData(CountData data) {
        synchronized (mCurrentCountDataCache) {
            mCurrentCountDataCache.add(data);
        }

        if (mCurrentCountDataCache.size() >= COUNT_SAVE_SIZE) {
            if (mCurrentCountDataCache == mCountDataCache1) {
                mCurrentCountDataCache = mCountDataCache2;
                saveAndClearCache(mCountDataCache1);
            } else if (mCurrentCountDataCache == mCountDataCache2) {
                mCurrentCountDataCache = mCountDataCache1;
                saveAndClearCache(mCountDataCache2);
            }
        }
    }

    public void saveAccelerometerProcessedData(AccelerometerProcessedData data) {
        synchronized (mCurrentAccelerometerProcessedDataCache) {
            mCurrentAccelerometerProcessedDataCache.add(data);
        }

        if (mCurrentAccelerometerProcessedDataCache.size() >= ACCELEROMETER_PROCESSED_SAVE_SIZE) {
            if (mCurrentAccelerometerProcessedDataCache == mAccelerometerProcessedDataCache1) {
                mCurrentAccelerometerProcessedDataCache = mAccelerometerProcessedDataCache2;
                saveAndClearCache(mAccelerometerProcessedDataCache1);
            } else if (mCurrentAccelerometerProcessedDataCache == mAccelerometerProcessedDataCache2) {
                mCurrentAccelerometerProcessedDataCache = mAccelerometerProcessedDataCache1;
                saveAndClearCache(mAccelerometerProcessedDataCache2);
            }
        }
    }

    public void saveGravityData(GravityData data) {
        synchronized (mCurrentGravityDataCache) {
            mCurrentGravityDataCache.add(data);
        }

        if (mCurrentGravityDataCache.size() >= GRAVITY_SAVE_SIZE) {
            if (mCurrentGravityDataCache == mGravityDataCache1) {
                mCurrentGravityDataCache = mGravityDataCache2;
                saveAndClearCache(mGravityDataCache1);
            } else if (mCurrentGravityDataCache == mGravityDataCache2) {
                mCurrentGravityDataCache = mGravityDataCache1;
                saveAndClearCache(mGravityDataCache2);
            }
        }
    }

    public void saveLinearAccelerationData(LinearAccelerationData data) {
        synchronized (mCurrentLinearAccelerationDataCache) {
            mCurrentLinearAccelerationDataCache.add(data);
        }

        if (mCurrentLinearAccelerationDataCache.size() >= LINEAR_ACCELERATION_SAVE_SIZE) {
            if (mCurrentLinearAccelerationDataCache == mLinearAccelerationDataCache1) {
                mCurrentLinearAccelerationDataCache = mLinearAccelerationDataCache2;
                saveAndClearCache(mLinearAccelerationDataCache1);
            } else if (mCurrentLinearAccelerationDataCache == mLinearAccelerationDataCache2) {
                mCurrentLinearAccelerationDataCache = mLinearAccelerationDataCache1;
                saveAndClearCache(mLinearAccelerationDataCache2);
            }
        }
    }

    private void saveAndClearCache(List<?> dataCache) {
        Message msg = mHandler.obtainMessage(MSG_SAVE_AND_CLEAR_DATA, dataCache);
        mHandler.sendMessage(msg);
    }

    public int getMoveMode() {
        return mMoveMode;
    }

    public void setMoveMode(int moveMode) {
        mMoveMode = moveMode;
    }

    public int getSensorPartMode() {
        return mSensorPartMode;
    }

    public void setSensorPartMode(int mensorPartMode) {
        mSensorPartMode = mensorPartMode;
    }

    public void forceSaveCache() {
        saveAndClearCache(mCurrentAccelerometerDataCache);
        saveAndClearCache(mCurrentCountDataCache);
        saveAndClearCache(mCurrentAccelerometerProcessedDataCache);
    }

    public void setDataSaveSwitch(boolean isDataSave) {
        mIsDataSave = isDataSave;
    }

}
