package com.borqs.tensorflow.step;

import com.borqs.tensorflow.step.service.StepService;


public interface UpdateUiCallBack {
    /**
     * 更新UI步数
     *
     * @param sourceId 数据来源
     * @param stepCount 步数
     */
    void updateUi(StepService.SOURCE_ID sourceId, int stepCount);
}
