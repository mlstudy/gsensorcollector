from __future__ import print_function

import math

from IPython import display
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.1f}'.format

acc_processed_dataframe = pd.read_csv("/home/zhuxueli/tensor_flow/accelerometer/acc.csv", sep=",")

acc_processed_dataframe = acc_processed_dataframe.reindex(
    np.random.permutation(acc_processed_dataframe.index))

def preprocess_features(acc_processed_dataframe):

  selected_features = acc_processed_dataframe[
    ["peak",
     "trough",
     "period"]]

  selected_features["period"] = selected_features["period"].apply(lambda x: min(x, 280))
  selected_features["peak"] = selected_features["peak"].apply(lambda x: min(x, 20))
  selected_features["trough"] = selected_features["trough"].apply(lambda x: min(x, 12))

  describe = selected_features["peak"].describe()
  selected_features["peak"] = ((selected_features["peak"] - describe["mean"]) / describe["std"])

  describe = selected_features["trough"].describe()
  selected_features["trough"] = ((selected_features["trough"] - describe["mean"]) / describe["std"])

  describe = selected_features["period"].describe()
  selected_features["period"] = ((selected_features["period"] - describe["mean"]) / describe["std"])

  return selected_features

def preprocess_targets(acc_processed_dataframe):

  output_targets = pd.DataFrame()

  output_targets["acc_processed_is_step"] = (acc_processed_dataframe["flag"] == 1).astype(float)
  return output_targets

training_examples = preprocess_features(acc_processed_dataframe.head(8000))
training_targets = preprocess_targets(acc_processed_dataframe.head(8000))

validation_examples = preprocess_features(acc_processed_dataframe.tail(2500))
validation_targets = preprocess_targets(acc_processed_dataframe.tail(2500))

# Double-check that we've done the right thing.
print("Training examples summary:")
display.display(training_examples.describe())
print("Validation examples summary:")
display.display(validation_examples.describe())

print("Training targets summary:")
display.display(training_targets.describe())
print("Validation targets summary:")
display.display(validation_targets.describe())

dataframe = training_examples.copy()
dataframe["target"] = training_targets["acc_processed_is_step"];
print(dataframe.corr())

#plt.figure(figsize=(15, 6))
#plt.subplot(1, 2, 1)
#plt.scatter(training_examples["peak"], training_examples["trough"])
#plt.show()

#plt.subplot(1, 2, 1)
#_ = training_examples["period"].hist()
#plt.show()

def construct_feature_columns(input_features):
  """Construct the TensorFlow Feature Columns.

  Args:
    input_features: The names of the numerical input features to use.
  Returns:
    A set of feature columns
  """
  return set([tf.feature_column.numeric_column(my_feature)
              for my_feature in input_features])

def my_input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
  """Trains a linear regression model of one feature.

  Args:
    features: pandas DataFrame of features
    targets: pandas DataFrame of targets
    batch_size: Size of batches to be passed to the model
    shuffle: True or False. Whether to shuffle the data.
    num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
  Returns:
    Tuple of (features, labels) for next data batch
  """

  # Convert pandas data into a dict of np arrays.
  features = {key:np.array(value) for key,value in dict(features).items()}                                            

  # Construct a dataset, and configure batching/repeating
  ds = Dataset.from_tensor_slices((features,targets)) # warning: 2GB limit
  ds = ds.batch(batch_size).repeat(num_epochs)

  # Shuffle the data, if specified
  if shuffle:
    ds = ds.shuffle(10000)

  # Return the next batch of data
  features, labels = ds.make_one_shot_iterator().get_next()

  return features, labels

def train_linear_classifier_model(
    learning_rate,
    steps,
    batch_size,
    training_examples,
    training_targets,
    validation_examples,
    validation_targets):
  """Trains a linear regression model of one feature.
  
  In addition to training, this function also prints training progress information,
  as well as a plot of the training and validation loss over time.
  
  Args:
    learning_rate: A `float`, the learning rate.
    steps: A non-zero `int`, the total number of training steps. A training step
      consists of a forward and backward pass using a single batch.
    batch_size: A non-zero `int`, the batch size.
    training_examples: A `DataFrame` containing one or more columns from
      `acc_processed_dataframe` to use as input features for training.
    training_targets: A `DataFrame` containing exactly one column from
      `acc_processed_dataframe` to use as target for training.
    validation_examples: A `DataFrame` containing one or more columns from
      `acc_processed_dataframe` to use as input features for validation.
    validation_targets: A `DataFrame` containing exactly one column from
      `acc_processed_dataframe` to use as target for validation.
      
  Returns:
    A `LinearClassifier` object trained on the training data.
  """

  periods = 10
  steps_per_period = steps / periods
  
  # Create a linear classifier object.
  my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
  my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)  
  linear_classifier = tf.estimator.LinearClassifier(
      feature_columns=construct_feature_columns(training_examples),
      optimizer=my_optimizer
  )
  
  # Create input functions
  training_input_fn = lambda: my_input_fn(training_examples, 
                                          training_targets["acc_processed_is_step"], 
                                          batch_size=batch_size)
  predict_training_input_fn = lambda: my_input_fn(training_examples, 
                                                  training_targets["acc_processed_is_step"], 
                                                  num_epochs=1, 
                                                  shuffle=False)
  predict_validation_input_fn = lambda: my_input_fn(validation_examples, 
                                                    validation_targets["acc_processed_is_step"], 
                                                    num_epochs=1, 
                                                    shuffle=False)
  
  # Train the model, but do so inside a loop so that we can periodically assess
  # loss metrics.
  print("Training model...")
  print("LogLoss (on training data):")
  training_log_losses = []
  validation_log_losses = []
  for period in range (0, periods):
    # Train the model, starting from the prior state.
    linear_classifier.train(
        input_fn=training_input_fn,
        steps=steps_per_period
    )
    # Take a break and compute predictions.    
    training_probabilities = linear_classifier.predict(input_fn=predict_training_input_fn)
    training_probabilities = np.array([item['probabilities'] for item in training_probabilities])
    
    validation_probabilities = linear_classifier.predict(input_fn=predict_validation_input_fn)
    validation_probabilities = np.array([item['probabilities'] for item in validation_probabilities])
    
    training_log_loss = metrics.log_loss(training_targets, training_probabilities)
    validation_log_loss = metrics.log_loss(validation_targets, validation_probabilities)
    # Occasionally print the current loss.
    print("  period %02d : %0.2f" % (period, training_log_loss))
    # Add the loss metrics from this period to our list.
    training_log_losses.append(training_log_loss)
    validation_log_losses.append(validation_log_loss)
  print("Model training finished.")
  
  # Output a graph of loss metrics over periods.
  """plt.ylabel("LogLoss")
  plt.xlabel("Periods")
  plt.title("LogLoss vs. Periods")
  plt.tight_layout()
  plt.plot(training_log_losses, label="training")
  plt.plot(validation_log_losses, label="validation")
  plt.legend()
  plt.show()"""

  evaluation_metrics = linear_classifier.evaluate(input_fn=predict_validation_input_fn)
  print("AUC on the validation set: %0.2f" % evaluation_metrics['auc'])
  print("Accuracy on the validation set: %0.2f" % evaluation_metrics['accuracy'])

  validation_probabilities = linear_classifier.predict(input_fn=predict_validation_input_fn)
  # Get just the probabilities for the positive class
  validation_probabilities = np.array([item['probabilities'][1] for item in validation_probabilities])

  false_positive_rate, true_positive_rate, thresholds = metrics.roc_curve(
      validation_targets, validation_probabilities)
  plt.plot(false_positive_rate, true_positive_rate, label="our model")
  _ = plt.legend(loc=2)
  plt.show()

  return linear_classifier

def export_estimator(
    export_dir,
    estimator,
    feature_columns):
  """Exporting estimator model in export_dir

  Args:
    export_dir: dir path for exported file
    estimator: A 'estimator' object
    feature_columns: Feature describe for input columns

  Retures:
  Exporting file path
  """

  #feature_spec = tf.feature_column.make_parse_example_spec(feature_columns)

  """def serving_input_receiver_fn():

    feature_placeholders = {
        'peak': tf.placeholder(tf.float32, [None], name="peak_input"),
        'trough': tf.placeholder(tf.float32, [None], name="trough_input"),
        'period': tf.placeholder(tf.float32, [None], name="period_input"),
    }
    return tf.contrib.learn.InputFnOps(
        feature_placeholders,
        None,
        feature_placeholders)"""

    """serialized_tf_example = tf.placeholder(dtype=tf.string,
                                           shape=[1],
                                           name='input_example_tensor')
    receiver_tensors = {'examples': serialized_tf_example}
    features = tf.parse_example(serialized_tf_example, feature_spec)
    print(features)
    return tf.estimator.export.ServingInputReceiver(features, receiver_tensors)"""


  peak_placeholder = tf.placeholder(tf.float32, shape=[1], name='peak')
  trough_placeholder = tf.placeholder(tf.float32, shape=[1], name='trough')
  period_placeholder = tf.placeholder(tf.float32, shape=[1], name='period')
  input_fn = tf.estimator.export.build_raw_serving_input_receiver_fn({
      "peak": peak_placeholder,
      "trough": trough_placeholder,
      "period": period_placeholder})

  export_dir = estimator.export_savedmodel(export_dir,
                                     input_fn)

  return export_dir

linear_classifier = train_linear_classifier_model(
    learning_rate=0.002,
    steps=1000,
    batch_size=20,
    training_examples=training_examples,
    training_targets=training_targets,
    validation_examples=validation_examples,
    validation_targets=validation_targets)

export_estimator(
    "/home/zhuxueli/tensor_flow/accelerometer/export",
    linear_classifier,
    construct_feature_columns(training_examples))








